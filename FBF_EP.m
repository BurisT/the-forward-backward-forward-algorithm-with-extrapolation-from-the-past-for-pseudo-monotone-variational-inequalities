function [x,time,k,Error] = FBF_EP(M,p,alpha,x0,y0,lambda,iteration,epsilon, xsol)
% FBF
myF=@(x) (exp(-norm(x)^2)+ alpha)*(M*x+p);
k=0;
x=x0;
y=y0;
residual=norm(x-xsol);
Error = residual;
tic;
Fy0 = myF(y);
while  k < iteration && residual > epsilon
    y=project(x-lambda*Fy0);
    Fy1 = myF(y);
    % new iteration  
    x= y + lambda * (Fy0-Fy1);
    Fy0 = Fy1; %update Fy0
    residual=norm(x-xsol);
    Error = [Error, residual];
    k=k+1; 
end 
time=toc;
disp('----FBF-EP-----')
disp(['Total time = ',num2str(time)])
disp(['Number of iterations = ',num2str(k)])
disp(['Residual = ',num2str(residual)])
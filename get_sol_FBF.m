function [x] = get_sol_FBF(M,p,alpha,x0,lambda,iteration)
% FBF
myF=@(x) (exp((-norm(x)^2))+ alpha)*(M*x+p);
k=0;
x=x0;
while  k < iteration
    Fx=myF(x);
    y=project(x-lambda*Fx);
    Fy = myF(y);
    % new iteration  
    x= y + lambda * (Fx-Fy);
    k=k+1; 
end 

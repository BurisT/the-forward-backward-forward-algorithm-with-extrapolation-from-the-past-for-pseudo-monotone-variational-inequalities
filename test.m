close all;
clear all;
clc;

num_iter = 100;
Sat_count = 0;

for i = 1:num_iter

fprintf('Number of iteration = %d \n',i);

%% setting environment
alpha = 0.1;
%%%% 9 dimensions %%%%%%%%
%M = [1	-1	2	 0	 2	1	 3	 1	 2 ;
%    -1	10	-5	 9	-2	2	-3	 5	-5 ;
%     2	-5	 6	-3	 5	1	 5	 2	 8 ;
%     0	 9	-3	13	 0	1	 4	12	-1 ;
%     2	-2	 5	 0	14	5	14	 1	 7 ;
%     1	 2	 1	 1	 5	5	 6	-1	 1 ;
%     3	-3	 5	 4	14	6	31	 2	 7 ;
%     1	 5	 2	12	 1 -1	 2	29	15 ;
%     2	-5	 8	-1	 7	1	 7	15	21 ;];

%p = [ -1; 2; 1; 0; -1; -1; 2; 1; 0];

%%%% 10 dimensions %%%%%%%%
M=[ 15  -5  10   0  10   5  15   5  10   0;
    -5  37  -8  18  -2   5  -3  11  -8   0;
    10  -8  14  -3   7   1   3   6  14   9;
     0  18  -3  34   0  -2  10  21   2   0;
    10  -2   7   0  21   6  17   0   7  11;
     5   5   1  -2   6   5   6  -1   1   1;
    15  -3   3  10  17   6  31   2   7   3;
     5  11   6  21   0  -1   2  29  15   6;
    10  -8  14   2   7   1   7  15  56  10;
     0   0   9   0  11   1   3   6  10  41];
 
 p = [1; 2; 1; 0; -1; 2; 0; 1; -1; 2];%ones(10,1);%[5; 1; 0; -2; -1; 0; -5; 4; -5; -1];%1 %[4; -1; 1; 4; 4; 5; -3; -3; 4; 1];%2%  [2; -2; 0; 1; -4; 1; 2; -1; 4; 3]; %3%  [-2; -1; -1; 3; 3; -1; 2; 5; 3; 2];%4% [1; 2; 1; 0; -1; 2; 0; 1; -1; 2];%5%%[2; -2; 0; 1; -4; 1; 2; -1; 4; 3];%2 %%  %[1; 2; 1; 0; -1; 2; 0; 1; -1; 2];%randi([-5,5],10,1);%ones(10,1);%old->%[ -1; 2; 1; 0; -1; -1; 2; 1; 0; -1];%[-1; 2; 1; 0; -1; 2; 0; 1; -1; 2];

%%%% 5 dimensions %%%%%%%%
%M=[5 -1 2 0 2;
%-1 6 -1 3 0;
%2 -1 3 0 1;
%0 3 0 5 0;
%2 0 1 0 4];

%p= [-1; 2; 1; 0;-1];
%%%%%%%%%%%%%%%%%%%%%%%%%%


% Fx = (exp(-norm(x)^2)+ alpha)*(M*x+p);
% grad Fx = 2*exp(-norm(x)^2*(M*x+p)' + (exp(-norm(x)^2)+ alpha)*M

% estimate L by upper bound of norm(grad Fx) over C

L= 2*sqrt(5)*(norm(M)*sqrt(5)+norm(p)) + (1+alpha)*norm(M);

lambda = 0.49/L;
lambda0 = 1; %0.008;
%mu=0.49;
%x0 = [1;3;2;1;4]; %5 dimension
%r = randi([-5 5],10,1);%-5 +10*rand(10,1);%randi([-5 5],10,1);
x0 =  randi([-10 10],10,1); %[-1; 2; 5; 3; 0; 3; -1; 5; 5; 4]; %[2; 1; -1; 10; 11; -3; 1; 5; -4; 1];
iteration = 10000;
epsilon = 10^(-6);
%y0 = [0;0;0;0;0]; %[-100;-100;-100;-100;-100];%[1;3;2;1;4]; %[0;0;0;0;0];
%%Choose for FBF_EP %5 dimension
y0 = randi([-10 10],10,1); %[8; -10; -8; 5; -2; -2; 2; -10; -2; -9]; %randi([-10 10],10,1); %[-1; 0; -3; 3; 4; 5; 1; 1; -4; 4];% randi([-5 5],10,1);%r;%[0;0;0;0;0;0;0;0;0;0];

%% monitor parameters
    fprintf('-----------------\n');
    fprintf('The vector x0 is: [');
    fprintf('%g, ', x0(1:end-1));
    fprintf('%g]\n', x0(end));
    
    fprintf('The vector p is: [');
    fprintf('%g, ', p(1:end-1));
    fprintf('%g]\n', p(end));
    
    fprintf('The vector y0 is: [');
    fprintf('%g, ', y0(1:end-1));
    fprintf('%g]\n', y0(end));


%% run Algorithm

[xsol] = get_sol_FBF(M,p,alpha,x0,lambda,iteration);


[x_EP,time_EP,k_EP,Error_EP] = FBF_EP(M,p,alpha,x0,y0,lambda,iteration,epsilon, xsol);


[x,time,k,Error] = linear_VI_FBF(M,p,alpha,x0,lambda,iteration,epsilon, xsol);


%[a_x,a_time,a_k,a_Error] = aFBF(M,p,alpha,x0,mu,lambda0,iteration,epsilon, xsol);

%[a_x_EP,a_time_EP,a_k_EP,a_Error_EP] = aFBF_EP(M,p,alpha,x0,y0,mu,lambda0,iteration,epsilon, xsol);

    h=figure;
    fig=gcf;
    set(fig,'DefaultTextInterpreter', 'latex');
    
    time_unit=time/k;
    time_unit_EP=time_EP/k_EP;
    
    %a_time_unit=a_time/a_k;
    %a_time_unit_EP=a_time_EP/a_k_EP;
    
    axis_x=linspace(0,k+1,k+1)*time_unit;
    axis_x_EP=linspace(0,k_EP+1,k_EP+1)*time_unit_EP;
    
    %a_axis_x=linspace(0,a_k+1,a_k+1)*a_time_unit;
    %a_axis_x_EP=linspace(0,a_k_EP+1,a_k_EP+1)*a_time_unit_EP;
    semilogy(axis_x,Error,'--','linewidth',2, 'color','r');
    hold on;
    semilogy(axis_x_EP,Error_EP,'linewidth',2, 'color','b');
    
    %semilogy(a_axis_x,a_Error,':','linewidth',2, 'color','g');
    %semilogy(a_axis_x_EP,a_Error_EP,'-.','linewidth',2, 'color','black');
    
    saveas(h,sprintf('FIG%d.png',i)) %save pics
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    xlabel('Elapsed time [sec]')
    ylabel('$\| x- x^* \|$','interpreter','latex')
    legend('FBF','FBF-EP')
    legend('FBF','FBF-EP','aFBF','aFBF-EP')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('-----------------\n');
    if time_unit_EP <= time_unit
        fprintf('Satisfaction : Yes\n');
        Sat_count = Sat_count + 1;
    else
        fprintf('Satisfaction : No\n');
    end
    
    fprintf('--------********************---------\n');
end

    fprintf('-----------------\n');
    fprintf('Number of Satisfaction %d from total %d \n',Sat_count,num_iter );
    fprintf('--------********************---------\n');
    %%%%%%%%%%%%%%%%%%%%%%%%%%

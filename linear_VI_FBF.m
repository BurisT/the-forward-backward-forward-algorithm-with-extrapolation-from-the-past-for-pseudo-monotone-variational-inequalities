function [x,time,k,Error] = linear_VI_FBF(M,p,alpha,x0,lambda,iteration,epsilon, xsol)
% FBF
myF=@(x) (exp(-norm(x)^2)+ alpha)*(M*x+p);
k=0;
x=x0;
residual = norm(x-xsol);
Error = residual;
tic;
while  k < iteration && residual > epsilon
    Fx=myF(x);
    y=project(x-lambda*Fx);
    Fy = myF(y);
    % new iteration  
    x= y + lambda * (Fx-Fy);
    residual=norm(x-xsol);
    Error = [Error, residual];
    k=k+1; 
end 
time=toc;
disp('----FBF-----')
disp(['Total time = ',num2str(time)])
disp(['Number of iterations = ',num2str(k)])
disp(['Residual = ',num2str(residual)])
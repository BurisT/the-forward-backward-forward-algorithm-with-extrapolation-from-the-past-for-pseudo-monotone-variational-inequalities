%project a vector on a<= <x,u> <=b
function [Px] = project(x)
[n,~]=size(x);
lb=zeros(n,1);
ub=5*ones(n,1);
H=eye(n);
%Px=max(lb,min(x,ub));
A = ones(1,n);
b = 5;
options = optimset('Display', 'off');
Px = quadprog(H,-x,A,b,[],[],lb,ub,x,options);
end
    
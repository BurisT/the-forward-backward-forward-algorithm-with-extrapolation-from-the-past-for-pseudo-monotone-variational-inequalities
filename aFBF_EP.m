function [x,time,k,Error] = aFBF_EP(M,p,alpha,x0,y0,mu,lambda,iteration,epsilon, xsol)
% FBF
myF=@(x) (exp(-norm(x)^2)+ alpha)*(M*x+p);
k=0;
x=x0;
y=y0;
residual=norm(x-xsol);
Error = residual;
tic;
Fy0 = myF(y);
while  k < iteration && residual > epsilon
    y=project(x-lambda*Fy0);
    Fy = myF(y);
    %fprintf('Fx=%f, Fy=%f, (Fx-Fy)=%f\n',Fy0, Fy,Fy0-Fy);
    %adative setpsize
    if (Fy0-Fy) ~= zeros(5,1) 
        lambda = min((mu*norm(y0-y)/norm(Fy0-Fy)),lambda);
        %fprintf('lambda change = %f (cadidate=%f)\n', lambda,mu*norm(y0-y)/norm(Fy0-Fy));
    else
        lambda = lambda;
    %     fprintf('Dont lambda change = %f\n', lambda);
    end
    % new iteration  
    x= y + lambda * (Fy0-Fy);
    Fy0 = Fy; %update Fy0
    residual=norm(x-xsol);
    Error = [Error, residual];
    k=k+1; 
end 
time=toc;
disp('----aFBF-EP-----')
disp(['Total time = ',num2str(time)])
disp(['Number of iterations = ',num2str(k)])
disp(['Residual = ',num2str(residual)])
function [x,time,k,Error] = aFBF(M,p,alpha,x0,mu,lambda,iteration,epsilon, xsol)
% adative stepsize FBF
myF=@(x) (exp(-norm(x)^2)+ alpha)*(M*x+p);
k=0;
x=x0;
residual=norm(x-xsol);
Error = residual;
tic;
while  k < iteration && residual > epsilon
    
    %fprintf('-------------\n %d lambda=%f\n',k ,lambda);
    Fx=myF(x);
    y=project(x-lambda*Fx);
    Fy = myF(y);
    %fprintf('Fx=%f, Fy=%f, (Fx-Fy)=%f\n',Fx, Fy,Fx-Fy);
    % adative setpsize
    if (Fx-Fy) ~= zeros(5,1) 
        lambda = min((mu*norm(x-y)/norm(Fx-Fy)),lambda);
        fprintf('%d lambda change = %f (cadidate=%f)\n',k ,lambda,(mu*norm(x-y)/norm(Fx-Fy)));
        fprintf('%f is result\n',Fx-Fy)
    else
        lambda = lambda;
         fprintf('Dont change lambda  = %f\n', lambda);
         %fprintf('at %d'.k);
         %break;
    end
    % new iteration  
    x= y + lambda * (Fx-Fy);
    residual=norm(x-xsol);
    Error = [Error, residual];
    k=k+1; 
    
end 
time=toc;
disp('----aFBF-----')
disp(['Total time = ',num2str(time)])
disp(['Number of iterations = ',num2str(k)])
disp(['Residual = ',num2str(residual)])